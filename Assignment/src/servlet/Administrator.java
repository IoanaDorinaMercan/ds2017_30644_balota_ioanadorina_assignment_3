package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.DVD;
import mom.Receiver;
import mom.Sender;

/**
 * Servlet implementation class Administrator
 */
public class Administrator extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Administrator() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>\n" + "<head><title>Admin Options</title></head>" + "<body>"
				+ "<h1>Administrator Options</h1>" + "<h2>Introduce informations about DVD</h2>" + "<br></br>" +

				"<form method=post action=Administrator>" + "Title	" + "<input type=text name=title>" + "<br>"
				+ "Year	 " + "<input type=text name=year>" + "<br>" + "Price " + "<input type=text name=price>" + "<br>"
				+ "Your Gmail" + "<input type=text name=email>" + "<br>" + "Your Password"
				+ "<input type=password name=password>" + "<br>"
				+ "<button type=submit name=option value=add>Add new DVD</button>" + "</form>" + "</html>" + "</body>");

		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String title = request.getParameter("title");
		int year = 0;
		double price = 0;
		try {
			year = Integer.parseInt(request.getParameter("year"));
			if (year < 0) {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<html>\n" + "<head><title>Admin Options</title></head>" + "<body>"
						+ "<h1>Administrator Options</h1>" + "<h2>Introduce informations about DVD</h2>" + "<br></br>" +

						"<form method=post action=Administrator>" + "Title	" + "<input type=text name=title>" + "<br>"
						+ "Year	 " + "<input type=text name=year>" + "<br>" + "Price " + "<input type=text name=price>"
						+ "<br>" + "Your Gmail" + "<input type=text name=email>" + "<br>" + "Your Password"
						+ "<input type=password name=password>" + "<br>"
						+ "<button type=submit name=option value=add>Add new DVD</button>" + "</form>" + "</html>"
						+ "</body>");

				out.close();
			}
		} catch (Exception e) {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>\n" + "<head><title>Admin Options</title></head>" + "<body>"
					+ "<h1>Administrator Options</h1>" + "<h2>Introduce informations about DVD</h2>" + "<br></br>"
					+ "<form method=post action=Administrator>" + "Title	" + "<input type=text name=title>" + "<br>"
					+ "Year	 " + "<input type=text name=year>" + "<br>" + "Price " + "<input type=text name=price>"
					+ "<br>" + "Your Gmail" + "<input type=text name=email>" + "<br>" + "Your Password"
					+ "<input type=password name=password>" + "<br>"
					+ "<button type=submit name=option value=add>Add new DVD</button>" + "</form>" + "</html>"
					+ "</body>");

			out.close();
		}

		try {
			price = Double.parseDouble(request.getParameter("price"));
			if (price < 0) {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();
				out.println("<html>\n" + "<head><title>Admin Options</title></head>" + "<body>"
						+ "<h1>Administrator Options</h1>" + "<h2>Introduce informations about DVD</h2>" + "<br></br>" +

						"<form method=post action=Administrator>" + "Title	" + "<input type=text name=title>" + "<br>"
						+ "Year	 " + "<input type=text name=year>" + "<br>" + "Price " + "<input type=text name=price>"
						+ "<br>" + "Your Gmail" + "<input type=text name=email>" + "<br>" + "Your Password"
						+ "<input type=password name=password>" + "<br>"
						+ "<button type=submit name=option value=add>Add new DVD</button>" + "</form>" + "</html>"
						+ "</body>");

				out.close();
			}
		} catch (Exception e) {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>\n" + "<head><title>Admin Options</title></head>" + "<body>"
					+ "<h1>Administrator Options</h1>" + "<h2>Introduce informations about DVD</h2>" + "<br></br>" +

					"<form method=post action=Administrator>" + "Title	" + "<input type=text name=title>" + "<br>"
					+ "Year	 " + "<input type=text name=year>" + "<br>" + "Price " + "<input type=text name=price>"
					+ "<br>" + "Your Gmail" + "<input type=text name=email>" + "<br>" + "Your Password"
					+ "<input type=password name=password>" + "<br>"
					+ "<button type=submit name=option value=add>Add new DVD</button>" + "</form>" + "</html>"
					+ "</body>");

			out.close();
		}

		DVD d = new DVD();
		d.setTitle(title);
		d.setPrice(price);
		d.setYear(year);
		System.out.println(d.getTitle() + " " + d.getYear() + " " + d.getPrice());
		String emailFrom = request.getParameter("email");
		String password = request.getParameter("password");
		Receiver r = new Receiver(emailFrom, password);
		r.receive();

		Sender s = new Sender();
		s.send(d);
		s.close();

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>\n" + "<head><title>Success</title></head>" + "<body>" + "<h1>Success!</h1>"
				+ "<h2>A new DVD was added!</h2>" + "<br></br>"
				+ "<h3>All your subscribers will receive an email with this new DVD.</h3>" + "<br></br>" + "</html>"
				+ "</body>");

		out.close();
	}

}
