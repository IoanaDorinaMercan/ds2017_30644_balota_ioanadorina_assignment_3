package mom;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;
import org.apache.commons.lang.SerializationUtils;
import com.rabbitmq.client.AMQP;
import service.FileService;
import service.MailService;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import entities.DVD;

public class Receiver {

	private static final String EXCHANGE_NAME = "logs";
	private Channel channel;
	private Queue q;
	private static final String subscribersFile = "../Documents/ds2017_30644_balota_ioanadorina_assignment_3/subscribers.txt";
	private static String emailFrom ;
	private static String password ;

	public Receiver(String emailFrom,String password) {
		this.emailFrom=emailFrom;
		this.password=password;
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost("localhost");

		com.rabbitmq.client.Connection connection = null;
		try {
			connection = factory.newConnection();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		channel = null;
		try {
			channel = connection.createChannel();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		} catch (IOException e) {
			e.printStackTrace();
		}

		q = new Queue(EXCHANGE_NAME, channel);

	}

	public void receive() {
		Consumer consumer = new DefaultConsumer(channel) {
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {

				DVD message = (DVD) SerializationUtils.deserialize(body);

				FileService file = new FileService();
				file.saveMessage("\n Title: " + message.getTitle() + "\n" + "Year: " + message.getYear() + "\n"
						+ "Price: " + message.getPrice());

				List<String> subscribers = file.readFile(subscribersFile);
				MailService mailService = new MailService(emailFrom, password);
				for (int i = 0; i < subscribers.size(); i++)
					mailService.sendMail(subscribers.get(i), "new DVD", "\nTitle: " + message.getTitle() + "\n"
							+ "Year: " + message.getYear() + "\n" + "Price: " + message.getPrice());

				System.out.println(" [" + consumerTag + "] Received ");
			}
		};

		try {
			channel.basicConsume(q.getQueueName(), true, consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
