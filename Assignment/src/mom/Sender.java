package mom;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

public class Sender {
	private static final String EXCHANGE_NAME = "logs";
	Channel channel;
	com.rabbitmq.client.Connection connection;

	public Sender() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		connection = null;
		try {
			try {
				connection = factory.newConnection();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		try {
			channel = connection.createChannel();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void send(Serializable object) {

		try {
			channel.basicPublish(EXCHANGE_NAME, "", null, SerializationUtils.serialize(object));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void close() {

		try {
			try {
				channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		try {
			connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
