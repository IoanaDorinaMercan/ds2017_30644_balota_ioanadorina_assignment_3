package mom;
import java.io.IOException;

import com.rabbitmq.client.Channel;

public class Queue {

	private String queueName;
	
	public Queue(String exchangeName,Channel channel)
	{
		
		try {
			queueName = channel.queueDeclare().getQueue();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			channel.queueBind(queueName, exchangeName, "");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	
	
}
