package service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileService {

	public void saveMessage(String content) {
		String path = "../Documents/ds2017_30644_balota_ioanadorina_assignment_3/" + "message-"
				+ System.currentTimeMillis() + ".txt";
		File file = new File(path);
		FileWriter fw = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		try {
			bw.write(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public List<String> readFile(String file) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(file));
		List<String>list=new ArrayList<String>();
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
		    list.add(line);

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    String everything = sb.toString();
		} finally {
		    br.close();
		}
		return list;
	}
}
